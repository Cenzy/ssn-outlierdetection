package ssnmodel;

import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.*;
/**
 * SSNModel
 * @author Vincenzo Digeno
 * @author v.digeno@studenti.uniba.it
 *
 */

public class SSNModel {
	public static final String SSN_URI = "http://www.w3.org/ns/ssn/"; 
	public static final String SOSA_URI = "http://www.w3.org/ns/sosa/";
	public static final String OBSERVATION_CLASS_URI = SOSA_URI + "Observation";
	public static final String SENSOR_CLASS_URI = SOSA_URI + "Sensor";
	public static final String MADE_BY_SENSOR_URI = SOSA_URI + "madeBySensor";
	public static final String MADE_OBSERVATION_URI = SOSA_URI + "madeObservation";
	public static final String OBSERVED_PROPERTY_URI = SOSA_URI + "observedProperty";
	public static final String RESULT_TIME_URI = SOSA_URI + "resultTime";
	//private static final String HAS_RESULT_URI = SOSA_URI + "hasResult";
	public static final String HAS_SIMPLE_RESULT_URI = SOSA_URI + "hasSimpleResult";
	public static final String PLATFORM_URI = SOSA_URI + "Platform";
	public static final String HOSTS_URI = SOSA_URI + "hosts";
	
	private OntModel ssnModel;

	private SSNModel() {
		this.ssnModel = ModelFactory.createOntologyModel();
		this.load("file:///home/vincenzo/Scaricati/sosa.ttl", "TTL");
	}
	
	public static SSNModel createSSNModel() {
		return new SSNModel();
	}
	
	/**
	 * Load the dataset.
	 * @param location Where the file is located
	 * @param type Serialization's type of the file
	 */
	public void load(String location, String type) {		
		OntDocumentManager dm = this.ssnModel.getDocumentManager();
		dm.addAltEntry("http://www.w3.org/ns/sosa/", location);
		
		this.ssnModel.read(location, type);
	}
	
	public OntClass getObservationClass() {
		return this.ssnModel.getOntClass(OBSERVATION_CLASS_URI);
	}
	
	public OntClass getSensorClass() {
		return this.ssnModel.getOntClass(SENSOR_CLASS_URI);
	}
	
	public ObjectProperty getObservedProperty() {
		return this.ssnModel.getObjectProperty(OBSERVED_PROPERTY_URI);
	}
	
	public ObjectProperty getMadeBySensor() {
		return this.ssnModel.getObjectProperty(MADE_BY_SENSOR_URI);
	}
	
	public ObjectProperty getMadeObservation() {
		return this.ssnModel.getObjectProperty(MADE_OBSERVATION_URI);
	}
	
	public DatatypeProperty getHasSimpleResultProperty() {
		return this.ssnModel.getDatatypeProperty(HAS_SIMPLE_RESULT_URI);
	}
	
	public DatatypeProperty getResultTime() {
		return this.ssnModel.getDatatypeProperty(RESULT_TIME_URI);
	}
	
	public ObjectProperty getHosts() {
		return this.ssnModel.getObjectProperty(HOSTS_URI);
	}
	
	public OntClass getPlatform() {
		return this.ssnModel.getOntClass(PLATFORM_URI);
	}
	
}
