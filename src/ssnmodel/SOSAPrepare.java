package ssnmodel;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;

import storm.SemanticStorm;

/**
 * This class prepares the data to be processed by the outlier detection method.
 * The main purpose is to filter the observation by an observation property,
 * beacause the method of outlier detection is univariate.
 * @author vincenzo
 *
 */
public class SOSAPrepare{

	/**
	 * List of observations under inspection.
	 */
	List<Observation> observations;
	
	/**
	 * Original model.
	 */
	private OntModel annotations;
	
	/**
	 * Filtered model.
	 */
	private Model annotationsFiltered;
	
	public SOSAPrepare(OntModel annotations) {
		this.annotations = annotations;
	}
	
	/**
	 * Filter observation by observable property for which the outlier detection is performed. 
	 * @param observablePropertyURI The URI of the observable property.
	 * @param observablePropertyName The name of the observable property.
	 */
	public void filterObservations(String observablePropertyURI, String observablePropertyName) {
		System.out.println("Filtering...");
		annotationsFiltered = this.executeConstruct(observablePropertyURI);
		this.executSelect(observablePropertyURI, observablePropertyName);
	}
	
	/**
	 * Execute the construct query.
	 * It filters the observation by the observable property.
	 * @param observablePropertyURI The URI of the observable property.
	 * @return The model with the observations.
	 */
	private Model executeConstruct(String observablePropertyURI) {
		final String queryConstruct =
				"prefix sosa: <http://www.w3.org/ns/sosa/>\n\n" +
				"construct\n" + 
			    "where{\n" +
						"?obs a sosa:Observation;\n" +
						"     sosa:madeBySensor ?sensor;\n" +
						"     sosa:observedProperty "+ observablePropertyURI + ";\n" +
						"     sosa:resultTime ?time;\n" +
						"     sosa:hasSimpleResult ?val.\n" +
						//"?p   sosa:hosts ?sensor.\n" +
						"}";
		
	    final Query query = QueryFactory.create(queryConstruct);
	    final QueryExecution qexec = QueryExecutionFactory.create(query, annotations);
	    return qexec.execConstruct();
	}
	
	/**
	 * Execute the select query.
	 * It gets the observation requested and add them on a list.
	 * @param observablePropertyURI The URI of the observable property.
	 * @param observablePropertyName The name of the observable property.
	 */
	private void executSelect(String observablePropertyURI, String observablePropertyName) {
		observations = new ArrayList<>();
		final String querySelect = 
				"prefix sosa: <http://www.w3.org/ns/sosa/>\n\n"+
				"select ?obs ?sensor ?time ?val where{\n"+
						"?obs a sosa:Observation;\n" +
						"     sosa:madeBySensor ?sensor;\n" +
						"     sosa:observedProperty "+ observablePropertyURI + ";\n" +
						"     sosa:resultTime ?time;\n" +
						"     sosa:hasSimpleResult ?val.\n" +
						"}";
		
		final QueryExecution exec = QueryExecutionFactory.create(querySelect, annotations);
		final ResultSet rs = exec.execSelect();
		while(rs.hasNext()) {
			final QuerySolution qs = rs.next();
			observations.add(new Observation(
					qs.get("obs").asResource(),
					qs.get("obs").toString(),
					qs.get("sensor").toString(),
					observablePropertyName,
					Instant.parse(qs.get("time").asLiteral().getLexicalForm()),
					qs.get("val").asLiteral().getDouble()));
		}
		
		Collections.sort(observations);
	}
	
	/**
	 * Trigger the outlier detection method calling Semantic Storm
	 * @param W Windows size
	 * @param R Radius
	 * @param k Number of neighbors
	 */
	public void outlierDetection(int W, double R, int k, boolean runEventDetection) {
		System.out.println("Outlier detection...");
		SemanticStorm ss = new SemanticStorm(W, R, k);
		try {
			ss.stream(annotationsFiltered, runEventDetection);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	
}
