package ssnmodel;

import java.time.Instant;

import org.apache.jena.rdf.model.Resource;

/**
 * Observation model a single observation.
 * An observation consists in: 
 * - its identification (uri);
 * - the sensor which performed the observation;
 * - the observed property;
 * - the instant in which the observation was made;
 * - the exact value of observation.
 * 
 * @author Vincenzo Digeno
 * @author v.digeno@studenti.uniba.it
 *
 */
public class Observation implements Comparable<Observation>{
	
	private Resource resource;
	private String uri;
	private String sensor;
	private String observableProperty;
	private Instant resultTime;
	private double value;
	private int nNeighs;
	
	public Observation() {
		
	}

	public Observation (Resource resource, String uri, String sensor, String observableProperty, Instant resultTime, double value) {
		this.resource = resource;
		this.uri = uri;
		this.sensor = sensor;
		this.observableProperty = observableProperty;
		this.resultTime = resultTime;
		this.value = value;
	}
	
	public Resource getResource() {
		return resource;
	}
	
	public String getURI() {
		return uri;
	}
	
	public String getSensor() {
		return sensor;
	}
	
	public String getObservableProperty() {
		return observableProperty;
	}

	public double getValue() {
		return value;
	}
	
	public Instant getResultTime() {
		return resultTime;
	}
	
	public int getNNeighs() {
		return this.nNeighs;
	}
	
	public void setResource(Resource resource) {
		this.resource = resource;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public void setSensor(String sensor) {
		this.sensor = sensor;
	}
	
	public void setObservableProperty(String observableProperty) {
		this.observableProperty = observableProperty;
	}
	
	public void setResultTime(Instant resultTime) {
		this.resultTime = resultTime;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	public void setNNehighs(int nNeighs) {
		this.nNeighs = nNeighs;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		Observation obs = (Observation) o;
		return 
				this.sensor == obs.getSensor() && 
				this.resultTime.equals(obs.getResultTime()) && 
				this.observableProperty == obs.getObservableProperty();
	}

	@Override
	public String toString() {
		return "Sensor " + this.sensor +
				" observed " + this.value + 
				" of " + this.observableProperty + 
				" on " + this.resultTime + 
				" with uri of obs: " + this.uri;
	}
	
	public int compareTo(Observation obs) {
		return this.resultTime.compareTo(obs.getResultTime());
	}
	
	/**
	 * The distance (manhattan) between two observation 
	 * @param obs1 The first observation
	 * @param obs2 The second observation
	 * @return
	 */
	public static double distance(Observation obs1, Observation obs2) {
		return Math.abs(obs1.value - obs2.value);
	}
	
}


