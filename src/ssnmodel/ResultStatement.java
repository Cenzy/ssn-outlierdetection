package ssnmodel;

import java.time.Instant;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.impl.StatementImpl;

public class ResultStatement extends StatementImpl implements Comparable<ResultStatement>{
	
	Instant datatime;

	public ResultStatement(Statement statement) {
		this(statement.getSubject(), statement.getPredicate(), statement.getObject());
		if(statement.getPredicate().getURI().equals(SSNModel.HAS_SIMPLE_RESULT_URI)) {
			this.datatime 
				= Instant.parse(statement.getSubject().getProperty(
							ResourceFactory.createProperty(SSNModel.RESULT_TIME_URI)).getLiteral().getLexicalForm());
		} else {
			System.out.println("Exception: it's not the value statement.");
			System.exit(0);
		}
	}
	
	public ResultStatement(Resource subject, Property predicate, RDFNode object) {
		super(subject, predicate, object);
	}
	
	public Instant getValue() {
		return this.datatime;
	}

	@Override
	public int compareTo(ResultStatement rs) {
		return datatime.compareTo(rs.getValue());
	}

	

	

}
