package test;

import ssnmodel.SOSAPrepare;

import java.io.IOException;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ModelFactory;

public class Nautilus{
    
  public static void main (String args[]) throws IOException {
	
	String filePath = "Data/Intel-Dataset/SOSA/partial-data(500outlier).ttl";
	String observationPropertyURL = "<http://www.example.org/temperature>";
	String observationPropertyName = "temperature";
	
	int W = 100;
	double R = 0.5;
	int k = 5;
	
	OntModel model = ModelFactory.createOntologyModel();
	model.read(filePath, "TTL");
	SOSAPrepare stream = new SOSAPrepare(model);
	stream.filterObservations(observationPropertyURL, observationPropertyName);
	stream.outlierDetection(W, R, k, false);
  }

}  
