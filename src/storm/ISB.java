package storm;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;

import ssnmodel.SSNModel;

/**
 * A data structure which manages the sliding window and performs the range query in order
 * to retrieve the observation neighbors.
 * @author Vincenzo Digeno
 * @author v.digeno@studenti.uniba.it
 */

public class ISB implements Iterable<Node>{

	/**
	 * Windows size
	 */
	private int W;
	
	/**
	 * Radius.
	 */
	private double R;
	
	/**
	 * A queue of nodes.
	 */
	private Queue<Node> nodes;
	
	/**
	 * The source model.
	 */
	private Model slidingModel;
	
	public ISB(Model model, int W, double R) {
		this.W = W;
		this.R = R;
		this.nodes = new ArrayDeque<>(W);
		this.slidingModel = ModelFactory.createDefaultModel();
	}
	
	/**
	 * Add the input statement into the sliding windows.
	 * When the windows slide, a new Statement enter and another
	 * exit. 
	 * The exit node is evaluated and if the number of his neighbors,
	 * computed during his entire life on the windows, are less then a 
	 * threshold (i.e. k), then it is an outlier and annotated  as "outlier" on the model.
	 * When a node is added, a range query is performed to retrieve his neighbors. This query is
	 * computed with a SPARQL query on the sliding model.
	 * @param s The added statement.
	 * @return The exit statement.
	 */
	public Statement add(Statement s) {
		Node nodeQuery = new Node(s);
		Statement expired;
		if(isempty()) {
			nodes.add(nodeQuery);
			slidingModel.add(nodeQuery.getStatement());
			return null;
		}
		
		if(isfull()) {
			Node nodeExp = nodes.poll();
			expired = nodeExp.getStatement();
			slidingModel.remove(expired);
			int nNeighs = nodeExp.getCount() + nodeExp.getNeighbors().size();
			expired = ResourceFactory.createStatement(
					expired.getSubject(),
					ResourceFactory.createProperty("http://www.example.org/hasNNeighs"),
					ResourceFactory.createTypedLiteral(new Integer(nNeighs)));
		} else {
			expired = null;
		}
		
		nodes.add(nodeQuery); 
		slidingModel.add(nodeQuery.getStatement());
		
		this.rangeQuerySearch(nodeQuery);
		
		
		return expired;
	}
	
	public Node remove() {
		return nodes.poll();
	}
	
	/**
	 * Remove all the remaining statement on the window structure.
	 * When no more statement is added, the window data structure contains
	 * the last W statement added. In order to clean the window and add
	 * the remaining statement on the model, they are removed and annotated.
	 * @return
	 */
	public List<Statement> getRemainingStatements(){
		List<Statement> statements = new ArrayList<>();
		while(!isempty()) {
			Statement expired;
			Node nodeExp = nodes.poll();
			expired = nodeExp.getStatement();
			slidingModel.remove(expired);
			int nNeighs = nodeExp.getCount() + nodeExp.getNeighbors().size();
			expired = ResourceFactory.createStatement(
					expired.getSubject(),
					ResourceFactory.createProperty("http://www.example.org/hasNNeighs"),
					ResourceFactory.createTypedLiteral(new Integer(nNeighs)));
			
			statements.add(expired);
		}
		
		return statements;
	}
	
	/**
	 * Compute a range query.
	 * Retrieve all neighbors of the query node n.
	 * @param n The query node.
	 */
	public void rangeQuerySearch(Node n) {
		List<Node> nodesArray = null;
		String query = 
				"prefix sosa: <http://www.w3.org/ns/sosa/>\n\n" + 
				"select ?obs \n" + 
				"where { \n" + 
				"<" + n.getStatement().getSubject().getURI() + "> sosa:hasSimpleResult ?nodevalue.\n" + 
				"?obs sosa:hasSimpleResult ?value.\n" + 
				"             FILTER(ABS(?nodevalue - ?value) <=" + R + ")\n" + 
				"} ";
		
		final QueryExecution qexec = QueryExecutionFactory.create(query, slidingModel);
		final ResultSet rs = qexec.execSelect();
		if(rs.hasNext()) {
			nodesArray = new ArrayList<>(nodes);
		}
		while(rs.hasNext()) {
			final QuerySolution qs = rs.next();
			Resource neighResource = qs.getResource("obs");
			Statement neighStatement = ResourceFactory.createStatement(neighResource, 
									      ResourceFactory.createProperty(SSNModel.HAS_SIMPLE_RESULT_URI), 
					                      ResourceFactory.createResource());
			int posNeigh = nodesArray.indexOf(new Node(neighStatement));
			Node neigh = nodesArray.get(posNeigh);
			neigh.incrementCount();
			n.addNeighbors(nodesArray.get(posNeigh).getStatement());
		}
		
	}
	
	/**
	 * Check if the windows is full.
	 * @return
	 */
	private boolean isfull() {
		return this.nodes.size() == W;
	}
	
	/**
	 * Check if the windows is empty.
	 * @return
	 */
	private boolean isempty() {
		return this.nodes.size() == 0;
	}

	@Override
	public Iterator<Node> iterator() {
		return nodes.iterator();
	}
}
