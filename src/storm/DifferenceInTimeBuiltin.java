package storm;

import org.apache.jena.reasoner.rulesys.BindingEnvironment;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.Util;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;

import java.time.Instant;

import org.apache.jena.graph.Node;

public class DifferenceInTimeBuiltin extends BaseBuiltin{

	@Override
	public String getName() {
		return "differenceInTime";
	}
	
    @Override
    public int getArgLength() {
        return 3;
    }
    
    public boolean bodyCall(Node[] args, int length, RuleContext context) {
    	checkArgs(length, context);
    	BindingEnvironment env = context.getEnv();
    	
    	Node t1 = getArg(0, args, context);
    	Node t2 = getArg(1, args, context);
    	
    	if(t1.isLiteral() && t2.isLiteral()) {
        	Instant t1Instant = Instant.parse(t1.getLiteralLexicalForm());
        	Instant t2Instant = Instant.parse(t2.getLiteralLexicalForm());
        	long differenceMillis = t1Instant.toEpochMilli() - t2Instant.toEpochMilli();
        	if(differenceMillis >= 0) {
            	long differenceSeconds = differenceMillis/1000;
            	return env.bind(args[2], Util.makeDoubleNode(differenceSeconds));
        	} else {
        		return false;
        	}

    	} else {
    		return false;
    	}

    }
        

}
