package storm;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.rulesys.BuiltinRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;
import ssnmodel.ResultStatement;
import ssnmodel.SSNModel;

/**
 * Direct the outlier detection method. 
 * This method take cue from ExactStorm, the mothod defined by Angiulli and Fassetti
 * (Detecting distance-based outliers in streams of data)
 * @author vincenzo
 *
 */
public class SemanticStorm {

	/**
	 * The ISB data structure.
	 */
	private ISB isb;
	
	/**
	 * Window size.
	 */
	private int W;
	
	/**
	 * Radius.
	 */
	private double R; 
	
	/**
	 * Minimum number of neighbors.
	 */
	private int k;
	
	public SemanticStorm(int W, double R, int k) {
		this.W = W;
		this.R = R;
		this.k = k;
		
		BuiltinRegistry.theRegistry.register(new DifferenceInTimeBuiltin());
	}
	
	/**
	 * Stream observation from a model and after the stream is done,
	 * retrieve observation which are outlier.
	 * @param model The source model.
	 * @return The model with outlier annotated.
	 */
	public Model stream(Model model, boolean runEventDetection) {
		this.isb = new ISB(model, W, R);
		
		Selector selector = new SimpleSelector(null, model.getProperty(SSNModel.HAS_SIMPLE_RESULT_URI), (RDFNode) null);
		StmtIterator iterator = model.listStatements(selector);
		List<ResultStatement> resultStatements = new ArrayList<>();
		while(iterator.hasNext()) {
			resultStatements.add(new ResultStatement(iterator.next()));
		}
		
		Collections.sort(resultStatements);
		for(ResultStatement rs : resultStatements) {
			Statement s = rs;
			Statement expired = isb.add(s);
			if(expired != null) {
				model.add(expired);
			}
		}
		
		//Retrieve observations remained in the window
		model.add(isb.getRemainingStatements());
		
		InfModel inf = this.detectOutliers(model);
		//Save the infer model with annotation about outlier
		this.saveModelToFile(inf, "dataset-inf.ttl");
		
		if(runEventDetection == true) {
			Model union = ModelFactory.createUnion(model, inf);
			InfModel infEv = this.detectEvents(union);
			this.saveModelToFile(infEv, "dataset-inf-event.ttl");
		}
		
		
		this.outlierAnalysis(inf, resultStatements.size());
		return inf;
	}
	
	/**
	 * Execute the inference rule to detect outlier observations
	 * @param model The source model
	 * @return The infer model
	 */
	private InfModel detectOutliers(Model model) {
		String rule = "[outlierRule: (?obs <http://www.example.org/hasNNeighs> ?val) lessThan(?val " 
				+ k + ") -> (?obs rdf:type <http://www.example.org/outlier>)]"; 
	
		List<Rule> rules = Rule.parseRules(rule);
		Reasoner reasoner = new GenericRuleReasoner(rules);
		InfModel inf = ModelFactory.createInfModel(reasoner, model);
		return inf;
	}
	
	/**
	 * Execute the inference rule to detect events
	 * @param model The source model
	 * @return The infer model
	 */
	private InfModel detectEvents(Model model) {		
		String event_rule = "[eventRule:" +
							"(?obs1 rdf:type <http://www.example.org/outlier>)" +
							"(?obs1 <http://www.w3.org/ns/sosa/madeBySensor> ?sensor1) " +
							"(?obs2 rdf:type <http://www.example.org/outlier>)" +
							"(?obs2 <http://www.w3.org/ns/sosa/madeBySensor> ?sensor2) " +
							"notEqual(?obs1, ?obs2), notEqual(?sensor1, ?sensor2)" +
							"(?platform1 <http://www.w3.org/ns/sosa/hosts> ?sensor1)" +
							"(?platform2 <http://www.w3.org/ns/sosa/hosts> ?sensor2)" +
							"equal(?platform1, ?platform2)" +
							"(?obs1 <http://www.w3.org/ns/sosa/resultTime>  ?t1) (?obs2 <http://www.w3.org/ns/sosa/resultTime>  ?t2)" +
							"differenceInTime(?t1, ?t2, ?dt) lessThan(?dt, 60)" +
							"-> (?obs1 <http://www.example.org/partOfEventWith> ?obs2)" +
							" (?obs2 <http://www.example.org/partOfEventWith> ?obs1)" +
							"]";
		
		List<Rule> rulesEvent = Rule.parseRules(event_rule);
		Reasoner reasonerEvent = new GenericRuleReasoner(rulesEvent);
		InfModel infEv = ModelFactory.createInfModel(reasonerEvent, model);
		
		final String querySelect = 
				"prefix sosa: <http://www.w3.org/ns/sosa/>\n\n"+
				"select distinct ?obs  where{\n"+
						"?obs <http://www.example.org/partOfEventWith> ?obs2\n" +
						"}";
		
		final QueryExecution exec = QueryExecutionFactory.create(querySelect, infEv);
		final ResultSet rs = exec.execSelect();
		while(rs.hasNext()) {
			final QuerySolution qs = rs.next();
			System.out.println(qs.get("obs"));			
		}
		
		return infEv;
	}
		
	/**
	 * Save the model with informations about outlier or event into a TTL file.
	 * @param inf The infer model
	 */
	private void saveModelToFile(Model inf, String fileName) {
		OutputStream out;
		try{
			System.out.println("I'm writing the infer model...");
			out = new FileOutputStream(fileName);
			RDFDataMgr.write(out, inf, Lang.TURTLE);
			out.close();
			System.out.println("Stop writing.");
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Analysis on the outlier method.
	 * @param inf The model under analysis. 
	 * @param numOfOb Numnber of observations.
	 * @throws IOException
	 */
	public void outlierAnalysis(Model inf, int numOfOb) {
		SimpleSelector sel = new SimpleSelector(null, RDF.type, ResourceFactory.createResource("http://www.example.org/outlier"));
		StmtIterator it = inf.listStatements(sel);
		int count = 0;
		
		while(it.hasNext()) {
			Statement st = it.next();
			String stSubjectURI = st.getSubject().getURI();
			String index =  stSubjectURI.substring(35, stSubjectURI.length());
			if(index.substring(index.length()-1).equals("T") || index.substring(index.length()-1).equals("O")) {
				index = index.substring(0, index.length() - 1);
			}
			count = count+1;
		}
		System.out.println("Analysis:");
		System.out.println("\nNumber of outlier:" + count + ". (Su " + numOfOb + ").");
	}
}	

