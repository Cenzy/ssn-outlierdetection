package storm;

import java.util.List;
import java.util.ArrayList;
import org.apache.jena.rdf.model.Statement;

/**
 * Node model a data structure that represents a single statement,
 * neighbors and the number of neighbors.
 * @author Vincenzo Digeno
 * @author v.digeno@studenti.uniba.it
 */

public class Node {
	/**
	 * A RDF statement, consisting in Subject, Predicate and Object.
	 */
	private Statement statement;
	
	/**
	 * Number of after neighbors.
	 */
	private int count;
	
	/**
	 * List of the previous neighbors.
	 */
	private List<Statement> neighbors;

	public Node(Statement statement) {
		this.statement = statement;
		this.count = 0;
		this.neighbors = new ArrayList<>();
	}

	public Statement getStatement() {
		return statement;
	}

	public int getCount() {
		return count;
	}

	public List<Statement> getNeighbors() {
		return neighbors;
	}
	
	public void incrementCount() {
		this.count+= 1;
	}
	
	public void addNeighbors(Statement resource) {
		this.neighbors.add(resource);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.statement.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof Node) {
			Node node = (Node) obj;
			return this.statement.getSubject().getURI().equals(
					node.getStatement().getSubject().getURI());
		} else {
			return false;
		}
	}
}
